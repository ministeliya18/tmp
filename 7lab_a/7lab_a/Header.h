#ifndef HEADER_H
#define HEADER_H

#include <fstream>
#include <iostream>
#include <string>

using namespace std;

//����� "������"
class Car {
    int Motor_power; //�������� ������
public:
    int Get_Motor_power();

    static Car* In_Car(ifstream& ifst); //������� ����� ���������� � ������
    virtual void In_Data(ifstream& ifst) = 0; //����� ����������� ������� ����� ����������
                                              //� ������, ��� ����� ���������� ���
                                              //������� ������ �����
    virtual void Out_Data(int Motor_power, ofstream& ofst) = 0; //����� ����������� ������� ������ ����������
                                              //� ������, ��� ����� ���������� ���
                                              //������� ������ �����
    virtual void Multi_Method(Car* Other, ofstream& ofst) = 0;
    virtual void Truck_MM(ofstream& ofst) = 0;
    virtual void Bus_MM(ofstream& ofst) = 0;
protected:
    Car() {};
};

//����� "��������"
class Truck : public Car {
    int Load_cap; //����������������
public:
    void In_Data(ifstream& ifst); //������� ����� ���������� � ��������
    void Out_Data(int Motor_power, ofstream& ofst); //������� ������ ���������� � ���������
    void Multi_Method(Car* Other, ofstream& ofst);
    void Truck_MM(ofstream& ofst);
    void Bus_MM(ofstream& ofst);
    Truck() {};
};

//����� "�������"
class Bus : public Car {
    short int Passenger_cap; //��������������������
public:
    void In_Data(ifstream& ifst); //������� ����� ���������� �� ��������
    void Out_Data(int Motor_power, ofstream& ofst); //������� ������ ���������� �� ��������
    void Multi_Method(Car* Other, ofstream& ofst);
    void Bus_MM(ofstream& ofst);
    void Truck_MM(ofstream& ofst);
    Bus() {};
};

//���� ����������
struct Node {
    Node* Next, * Prev; //��������� �� ��������� � ���������� �������� ����������
    Car* Cont; //��������� �� ������
};

//���������
class Container {
    Node* Head, *Tail; //��������� �� "������" � "�����" ����������
    int Len; //����������� ����������
public:
    void In(ifstream& ifst); //������� ����� �������� � ���������
    void Out(ofstream& ofst); //������� ������ �������� �� ����������
    void Clear(); //������� �������� ����������
    void Multi_Method_Container(ofstream& ofst); //������� ������������
    Container(); //�����������
    ~Container() { Clear(); } //����������
};

#endif //HEADER_H