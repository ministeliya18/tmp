#include "Header.h"

Container::Container() {
    Head = Tail = new Node();
    Head->Cont = Tail->Cont = NULL;
    Head->Next = Tail->Next = NULL;
    Head->Prev = Tail->Prev = NULL;
    Len = 0;
}

void Container::In(ifstream& ifst) {
    Node* Temp;

    while (!ifst.eof()) {
        Temp = new Node(); //������� ����� ����
        //�������������� ����� ����
        Temp->Next = NULL;
        Temp->Prev = NULL;

        
        if (!Len) { //������ 1-�� ��������
            if ((Head->Cont = Car::In_Car(ifst))) {
                Tail = Head;
                Len++;
            }
        }
        else { //������ ����������� ���������
            if ((Temp->Cont = Car::In_Car(ifst))) {
                Tail->Next = Temp;
                Temp->Prev = Tail;
                Tail = Temp;
                Len++;
            }
        }
    }
}

void Container:: Out(ofstream& ofst) {
    ofst << "Container contains " << Len
        << " elements." << endl << endl;

    Node* Temp = Head;

    for (int i = 0; i < Len; i++) {
        ofst << i << ": ";
        Temp->Cont->Out_Data(Temp->Cont->Get_Motor_power(), ofst);

        if (Temp->Next) {
            Temp = Temp->Next;
        }
    }
}

void Container::Clear() {
    Node* Temp = Head;

    for (int i = 0; i < Len; i++) {
        free(Temp->Cont);

        if (Temp->Next) {
            Temp = Temp->Next;
            free(Temp->Prev);
        }

    }

    Len = 0;
}

void Container::Multi_Method_Container(ofstream& ofst) {
    Node* First = Head;
    Node* Second = Head->Next;

    ofst << "Multimethod." << endl << endl;

    for (int i = 0; i < Len - 1; i++) {
        for (int j = i + 1; j < Len; j++) {
            First->Cont->Multi_Method(Second->Cont, ofst);
            First->Cont->Out_Data(First->Cont->Get_Motor_power(), ofst);
            Second->Cont->Out_Data(Second->Cont->Get_Motor_power(), ofst);

            Second = Second->Next;
        }

        First = First->Next;
        Second = First->Next;
    }
}

Car* Car::In_Car(ifstream& ifst) {
    Car* C;
    int K;

    ifst >> K;
    
    if (K == 1) {
        C = new Truck;

        ifst >> C->Motor_power; //C�������� �������� ������
    }
    else if (K == 2) {
        C = new Bus;

        ifst >> C->Motor_power; //C�������� �������� ������
    }
    else {
        return 0;
    }

    C->In_Data(ifst);

    return C;
}

int Car::Get_Motor_power() {
    return Motor_power;
}

void Truck::In_Data(ifstream& ifst) {
    ifst >> Load_cap;
}

void Truck::Out_Data(int Motor_power, ofstream& ofst) {
    ofst << "It's a Truck with motor power = " << Motor_power << endl;
    ofst << "Its load capacity is " << Load_cap << endl << endl;
}

void Truck::Multi_Method(Car* Other, ofstream& ofst) {
    Other->Truck_MM(ofst);
}

void Truck::Truck_MM(ofstream& ofst) {
    ofst << "Truck and Truck." << endl;
}

void Truck::Bus_MM(ofstream& ofst) {
    ofst << "Truck and Bus." << endl;
}

void Bus::In_Data(ifstream& ifst) {
    ifst >> Passenger_cap;
}

void Bus::Out_Data(int Motor_power, ofstream& ofst) {
    ofst << "It's a Bus with motor power = " << Motor_power << endl;
    ofst << "Its passenger capacity is " << Passenger_cap << endl << endl;
}

void Bus::Multi_Method(Car* Other, ofstream& ofst) {
    Other->Bus_MM(ofst);
}

void Bus::Bus_MM(ofstream& ofst) {
    ofst << "Bus and Bus." << endl;
}

void Bus::Truck_MM(ofstream& ofst) {
    ofst << "Bus and Truck." << endl;
}