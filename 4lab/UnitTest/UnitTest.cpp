#include "pch.h"
#include "CppUnitTest.h"

#include "../4lab/Header.h"
#include "../4lab/Source.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTest
{
	TEST_CLASS(UnitTest)
	{
	public:
		
		TEST_METHOD(In_Truck_Test) //���� �������� ����� ���������� � ���������
		{
			ifstream ifst("../4lab/in_truck.txt");

			Truck* T = new Truck();

			T->In_Data(ifst);

			Assert::AreEqual(T->Get_Load_cap(), 1500);
		}
		TEST_METHOD(In_Bus_Test) //���� �������� ����� ���������� �� ��������
		{
			ifstream ifst("../4lab/in_bus.txt");

			Bus* B = new Bus();

			B->In_Data(ifst);

			Assert::AreEqual(B->Get_Passenger_cap(), (short int)54);
		}
		TEST_METHOD(In_Passenger_car_Test) //���� �������� ����� ���������� � �������� ����������
		{
			ifstream ifst("../4lab/in_passenger_car.txt");

			Passenger_car* P_c = new Passenger_car();

			P_c->In_Data(ifst);

			Assert::AreEqual(P_c->Get_Max_speed(), (short int)330);
		}
		TEST_METHOD(In_Car_Test) //���� �������� ����� ���������� � ������ 
		{
			ifstream ifst("../4lab/in_car.txt");

			Car* C = Car::In_Car(ifst);

			Assert::AreEqual(C->Get_Motor_power(), 100);
			Assert::AreEqual(C->Get_Fuel(), 32.3);
			
		}
		TEST_METHOD(Out_Truck_Test) //���� �������� ������ ���������� � ���������
		{
			Truck* T_act = new Truck();

			T_act->Set_Load_cap(1500);

			ofstream ofst("../4lab/out_truck_act.txt");

			T_act->Out_Data(100, 32.3, ofst);

			ifstream ifst_exp("../4lab/out_truck_exp.txt");
			ifstream ifst_act("../4lab/out_truck_act.txt");

			string Expected;
			getline(ifst_exp, Expected, '\0');
			string Actual;
			getline(ifst_act, Actual, '\0');

			Assert::AreEqual(Expected, Actual);
		}
		TEST_METHOD(Out_Bus_Test) //���� �������� ������ ���������� �� ��������
		{
			Bus* B_act = new Bus();

			B_act->Set_Passenger_cap(54);

			ofstream ofst("../4lab/out_bus_act.txt");

			B_act->Out_Data(110, 27.8, ofst);

			ifstream ifst_exp("../4lab/out_bus_exp.txt");
			ifstream ifst_act("../4lab/out_bus_act.txt");

			string Expected;
			getline(ifst_exp, Expected, '\0');
			string Actual;
			getline(ifst_act, Actual, '\0');

			Assert::AreEqual(Expected, Actual);
		}
		TEST_METHOD(Out_Passenger_car_Test) //���� �������� ������ ���������� � �������� ����������
		{
			Passenger_car* P_c_act = new Passenger_car();

			P_c_act->Set_Max_speed(330);

			ofstream ofst("../4lab/out_passenger_car_act.txt");

			P_c_act->Out_Data(135, 17.6, ofst);

			ifstream ifst_exp("../4lab/out_passenger_car_exp.txt");
			ifstream ifst_act("../4lab/out_passenger_car_act.txt");

			string Expected;
			getline(ifst_exp, Expected, '\0');
			string Actual;
			getline(ifst_act, Actual, '\0');

			Assert::AreEqual(Expected, Actual);
		}
		TEST_METHOD(Load_to_capacity_ratio_Test) //���� �������� ������� ������� ��������� ���������������� � ��������
		{
			Truck* T = new Truck;

			T->Set_Load_cap(1500);

			Car* C = T;

			double Ratio_exp = 15;
			double Ratio_act = C->Load_to_capacity_ratio(100);

			Assert::AreEqual(Ratio_exp, Ratio_act);
		}
		TEST_METHOD(Compare_Test) //���� �������� �����������
		{
			Truck* T = new Truck;

			T->Set_Load_cap(1500);

			Car* C_First = T;

			Bus* B = new Bus();

			B->Set_Passenger_cap(54);

			Car* Car_Second = B;

			bool Act = C_First->Compare(Car_Second);
			bool Exp = true;

			Assert::AreEqual(Exp, Act);
		}
		TEST_METHOD(Sort_Test) //���� �������� ����������
		{
			ifstream ifst("../4lab/input.txt");

			Container C; //���������� ����������

			C.In(ifst); //���� ��������� ����������

			C.Sort(); //���������� ����������

			ofstream ofst("../4lab/sort_act.txt");

			C.Out(ofst); //����� ����������

			ifstream ifst_exp("../4lab/sort_exp.txt");
			ifstream ifst_act("../4lab/sort_act.txt");

			string Exp;
			getline(ifst_exp, Exp, '\0');
			string Act;
			getline(ifst_act, Act, '\0');

			Assert::AreEqual(Exp, Act);
		}
	};
}
