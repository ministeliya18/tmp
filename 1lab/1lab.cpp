﻿// 1lab.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "Header.h"

int main(int argc, char* argv[]) {


    ifstream ifst("input.txt");
    ofstream ofst("output.txt");

    cout << "Start" << endl;

    Container C; //Объявление контейнера

    C.In(ifst); //Ввод элементов контейнера

    C.Sort(); //Сортировка контейнера

    ofst << "Filled and sorted container. " << endl;

    C.Out(ofst); //Вывод контейнера

    C.Out_Only_Truck(ofst); //Вывод только грузовиков

    C.Clear(); //Очистка контейнера


    ofst << "Empty container. " << endl;

    C.Out(ofst); //Вывод контейнера

    cout << "Stop" << endl;

    system("pause");
    return 0;
}